var positionInput;
var textInput;
var textParagraph;
var definedElements;
var nextIdIndex;

function start() {
    var add_button = document.getElementById("add_element_button");
    var remove_button = document.getElementById("remove_element_button");
    var change_button = document.getElementById("change_element_button");

    add_button.addEventListener("click", add_button_listener, false);
    remove_button.addEventListener("click", remove_button_listener, false);
    change_button.addEventListener("click", change_button_listener, false);

    positionInput = document.getElementById("position");
    textInput = document.getElementById("input_text");
    textParagraph = document.getElementById("text_paragraph");

    definedElements = [];
    nextIdIndex = 0;
}

function add_button_listener() {
    position = parseInt(positionInput.value);
    text = textInput.value;
    if (text != "") {
        newElement = document.createElement("div");
        newElement.appendChild(document.createTextNode(text));
        idText = "div" + nextIdIndex;
        newElement.id = idText;

        if (!isNaN(position) && definedElements.length > 0) {
            if (position < 0) {
                position = 0;
            } else if (position >= definedElements.length) {
                position = definedElements.length - 1;
            }
            refElement = document.getElementById(definedElements[position])
            textParagraph.insertBefore(newElement, refElement);
            definedElements.splice(position, 0, idText);
        }
        else {
            textParagraph.appendChild(newElement);
            definedElements.push(idText);
        }
        nextIdIndex++;
        console.log(definedElements);
    }
}

function remove_button_listener() {
    console.log("remove_element");
    position = parseInt(positionInput.value);
    if ((!isNaN(position)) && (definedElements.length > 0)) {
        if (position < 0) {
            position = 0;
        } else if (position >= definedElements.length) {
            position = definedElements.length - 1;
        }
        element = definedElements[position];
        definedElements.splice(position, 1);
        textParagraph.removeChild(document.getElementById(element));
    }
    else {
        elem = definedElements.pop();
        console.log("removed element id: " + document.getElementById(elem))
        textParagraph.removeChild(document.getElementById(elem));
    }
}

function change_button_listener() {
    console.log("remove_element");
    position = parseInt(positionInput.value);
    text = textInput.value;
    if (text != "") {
        newElement = document.createElement("div");
        newElement.appendChild(document.createTextNode(text));
        idText = "div" + nextIdIndex;
        newElement.id = idText;
        if ((!isNaN(position)) && (definedElements.length > 0)) {
            if (position < 0) {
                position = 0;
            } else if (position >= definedElements.length) {
                position = definedElements.length - 1;
            }
            element = definedElements[position];
            definedElements.splice(position, 1, idText);
            textParagraph.replaceChild(newElement, document.getElementById(element));
        }
        else {
            elem = definedElements.pop();
            definedElements.push(idText);
            console.log("removed element id: " + document.getElementById(elem));
            textParagraph.replaceChild(newElement, document.getElementById(elem));
        }
        nextIdIndex++;
    }
}



window.addEventListener("load", start, false);
document.writeln("<h1>Operacje</h1>")