﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Products : System.Web.UI.Page
{
    private Dictionary<string, CheckBoxList> productTypesFilterStringCheckBoxes = new Dictionary<string, CheckBoxList>();
    private string selectedItemType;
    protected void Page_Init(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("Page_Init");
        productTypesFilterStringCheckBoxes.Add("Kondensatory", products_list1);
        productTypesFilterStringCheckBoxes.Add("Rezystory", products_list2);
        productTypesFilterStringCheckBoxes.Add("Mikrokontrolery", products_list3);
        productTypesFilterStringCheckBoxes.Add("Półprzewodniki", products_list4);
        if(null == Session["product_list"])
        {
            Session.Add("product_list", new Dictionary<string, int>());
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("Page_Load");
        if (Session["amount_products"] == null)
        {
            Session.Add("amount_products", 0);
        }
        product_amount.Text = ((int)Session["amount_products"]).ToString();
    }

    protected void BtnAddProduct_Click(object sender, EventArgs e)
    {
        productTypesFilterStringCheckBoxes["Kondensatory"] = products_list1;
        productTypesFilterStringCheckBoxes["Rezystory"] = products_list2;
        productTypesFilterStringCheckBoxes["Mikrokontrolery"] = products_list3;
        productTypesFilterStringCheckBoxes["Półprzewodniki"] = products_list4;
        if (product_types.SelectedItem != null)
        {
            Session.Add("product_type", productTypesFilterStringCheckBoxes[product_types.SelectedItem.Text]);
            System.Diagnostics.Debug.WriteLine("BtnAddProduct_Click");
            CheckBoxList availableProductsList = (CheckBoxList)Session["product_type"];

            Dictionary<string, int> productsList = (Dictionary<string, int>)Session["product_list"];
            int amountProducts = (int)Session["amount_products"];
            for (int i = 0; i < availableProductsList.Items.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine("Loop " + i);
                if (availableProductsList.Items[i].Selected)
                {
                    if (!productsList.ContainsKey(availableProductsList.Items[i].Text))
                    {
                        productsList.Add(availableProductsList.Items[i].Text, 0);
                    }
                    System.Diagnostics.Debug.WriteLine("Before " + productsList[availableProductsList.Items[i].Text]);
                    productsList[availableProductsList.Items[i].Text] += 1;
                    System.Diagnostics.Debug.WriteLine("After " + productsList[availableProductsList.Items[i].Text]);
                    amountProducts++;
                    System.Diagnostics.Debug.WriteLine(amountProducts);
                }
            }
            foreach (KeyValuePair<string, int> entry in productsList)
            {
                System.Diagnostics.Debug.WriteLine("Key=" + entry.Key + " Value=" + entry.Value);

            }
            Session.Add("product_list", productsList);
            Session.Add("amount_products", amountProducts);

            product_amount.Text = amountProducts.ToString();
        }
    }


    protected void RadBtnProductType_Click(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("RadBtnProductType_Click");
        foreach (KeyValuePair<string, CheckBoxList> entry in productTypesFilterStringCheckBoxes)
        {
            entry.Value.Visible = false;
        }
        productTypesFilterStringCheckBoxes[product_types.SelectedItem.Text].Visible = true;
    }
}