﻿<%@ page language="C#" autoeventwireup="true" codefile="ShoppingSummary.aspx.cs" inherits="ShoppingSummary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="stylesheet" type="text/css" href="aspnet.css">
    <title></title>
</head>
<body>
    <div>
        <h1>Twój koszyk
        </h1>
    </div>
    <p>Dziękujemy za złożenie zamówienia</p>
    <p>
        Łączny koszt zamówienia:
        <asp:label id="order_cost" runat="server" visible="True"></asp:label>
    </p>
    <p>
        Wybrana forma dostarczenia zamówienia:
        <asp:label id="order_way" runat="server" visible="True"></asp:label>
    </p>
    <div>
        <asp:hyperlink id="toProducts" navigateurl="Products.aspx" text="Powrót do listy produktów" runat="server" />
    </div>



</body>
</html>
