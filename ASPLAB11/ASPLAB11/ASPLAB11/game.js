var questions = ["Który typ w C++ jest typem zmiennopozycyjnym podwójnej precyzji?",
    "Jaki znak realizuje dereferencję wskaźnika?",
    "Jaki znak oznacza parametr funkcji jako referencję?",
    "Jakie słowo kluczowe umożliwia automatyczną dedukcję typu w C++?"]

var answers = []

function Question(question, answers, correctA) {
    this.question = question;
    this.answers = answers;
    this.correctAnswer = correctA;
}


var q1 = new Question("Który typ w C++ jest typem zmiennopozycyjnym podwójnej precyzji?",
    ["double", "int", "long long", "float"], [0]);
var q2 = new Question("Jaki znak realizuje dereferencję wskaźnika?",
    ["&", "*", "!", "//"], [1]);
var q3 = new Question("Jaki znak oznacza parametr funkcji jako referencję?",
    ["&", "*", "!", "//"], [0]);
var q4 = new Question("Jakie słowo kluczowe umożliwia automatyczną dedukcję typu w C++?",
    ["Nie ma takiego", "dectype", "var", "auto"], [3]);
var q5 = new Question("W jakich obszarach pamięci program alokuje zmienne?",
    ["Na stosie", "Na półce", "W pudle", "Na stercie"], [0, 3]);
var q6 = new Question("Jakie C++ definiuje operacje rzutowania?",
    ["reinterprete_cast", "const_cast", "static_cast", "non_cast"], [0, 1, 2]);
var q7 = new Question("Słowa kluczowe w języku C++?",
    ["ini", "dectype", "var", "auto"], [1, 3]);


var questions = [q1, q2, q3, q4, q5, q6, q7];


var questionLabel;
var answersRadioButton;
var answersRadioLabel;
var scores;
var correctAnswersTab;
var acceptButton;
var currentQuestionIdx;
var scoresTab;


var selectedQuestions;
var selectedQuestionsNum;

function start() {
    selectedQuestionsNum = 7;
    currentQuestionIdx = 0;
    questionLabel = document.getElementById("pytanie");
    answersRadioButton = [document.getElementById("one"),
    document.getElementById("two"),
    document.getElementById("three"),
    document.getElementById("four")];
    answersRadioLabel = [document.getElementById("one_label"),
    document.getElementById("two_label"),
    document.getElementById("three_label"),
    document.getElementById("four_label")];
    acceptButton = document.getElementById("acceptButton");
    acceptButton.addEventListener("click", onAcceptButtonListener, false);
    scoresTab = document.getElementById("scoresTab");
    scores = 0;
    correctAnswersTab = [];
    questions = rand(questions, selectedQuestionsNum)
    loadQuestion();
}

function rand(arr, elem) {
    for (i = 0; i < elem; i++) {
        r = Math.floor((Math.random() * (7 - i) + i));
        tmp = arr[i];
        arr[i] = arr[r];
        arr[r] = tmp;
    }
    return arr;
}

function loadQuestion() {
    questionLabel.innerHTML = questions[currentQuestionIdx].question;
    i = 0;
    while (i < 4) {
        answersRadioLabel[i].innerHTML = questions[currentQuestionIdx].answers[i];
        i++;
    }
}

function onAcceptButtonListener() {
    checkedNum = 0;
    for (i = 0; i < 4; i++) {
        if (answersRadioButton[i].checked) {
            checkedNum++;
        }
    }

    if (checkedNum == questions[currentQuestionIdx].correctAnswer.length) {

        i = 0;
        while(i < questions[currentQuestionIdx].correctAnswer.length) {
            if (false == answersRadioButton[questions[currentQuestionIdx].correctAnswer[i]].checked) {
                correctAnswersTab[currentQuestionIdx] = false;
                break;
            }
            i++;
        }

        if (i == questions[currentQuestionIdx].correctAnswer.length) {
            scores++;
            correctAnswersTab[currentQuestionIdx] = true;
        }
    }
    currentQuestionIdx++;
    scoresTab.innerHTML = createTable();
    if (currentQuestionIdx >= (selectedQuestionsNum)) {
        showSummary();
    }
    else {
        loadQuestion(currentQuestionIdx)
    }
}

function getAllCorrect(i) {
    var result = ""
    for (j = 0; j < questions[i].correctAnswer.length; j++) {
        result += (" " + questions[i].answers[questions[i].correctAnswer[j]])
    }
    return result;
}

function createTable() {
    var table = new String("<table>" +
        "<caption>Wyniki dotychczasowych odpowiedzi</caption>" +
        "<thead><th>Nr. pytania</th><th>Czy poprawnie?</th><th>Poprawna odpowiedź</th></thead>" +
        "<tbody>");
    for (i = 0; i < currentQuestionIdx; i++) {
        table = table + "<tr><td>" + (i + 1) + "</td><td>" +
            (correctAnswersTab[i] ? "TAK" : "NIE") + "</td><td>" +
            getAllCorrect(i) + "</td></tr>";
    }
    return table;
}

function showSummary() {
    acceptButton.style.display = 'none';
    i = 0;
    do {
        answersRadioLabel[i].style.display = 'none';
        answersRadioButton[i].style.display = 'none';
        i++;
    } while (i < 4)
    document.getElementById("header_pytanie").style.display = 'none';
    questionLabel.innerHTML = "Twój wynik: " + scores + ".";
    switch (scores) {
        case 0:
            questionLabel.innerHTML += " Poćwicz trochę.";
            break;
        case 1:
            questionLabel.innerHTML += " Widać postępy!";
            break;
        case 2:
            questionLabel.innerHTML += " Jeszcze trochę, a będziesz mistrzem.";
            break;
        case 3:
            questionLabel.innerHTML += " Brakuje Ci oooo __ tyle.";
            break;
        case 4:
            questionLabel.innerHTML += " Jesteś mistrzem C++.";
            break;
        default:
        questionLabel.innerHTML += " Wyskoczyłeś ponad skalę.";
    }
}

window.addEventListener("load", start, false);
