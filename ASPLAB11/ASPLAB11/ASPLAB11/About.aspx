﻿<%@ page language="C#" autoeventwireup="true" codefile="About.aspx.cs" inherits="About" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="form.css">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:label id="label" runat="server" visible="True">Naciśnij poniższy przycisk w celu wyświetlenia informacji o witrynie</asp:label>
        </div>
        <div>
            <asp:button id="showInfo" text="Submit" runat="server" />
        </div>
        <div>
            <asp:label id="outputLabel" runat="server" visible="False"></asp:label>
        </div>
    </form>
</body>
</html>
