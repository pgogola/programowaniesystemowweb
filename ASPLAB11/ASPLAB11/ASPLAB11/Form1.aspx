﻿<%@ page language="C#" autoeventwireup="true" codefile="Form1.aspx.cs" inherits="Form1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="form.css">
    <script type="text/javascript">
        function goToMainAction() {
            if (window.confirm("Czy na pewno chcesz opóścić tę stronę?")) {
                window.location.href = "/";
            }
        }
    </script>

    <title></title>
</head>
<body>
    <h1 class="form_h1">Formularz 1</h1>
    <form id="form1" runat="server">
        <p>
            <asp:label id="userNameLabel"
                text="Username:"
                associatedcontrolid="userNameTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userNameTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="nameRequiredFieldValidator" runat="server"
                controltovalidate="userNameTextBox" display="Dynamic"
                errormessage="Please enter your name" forecolor="Red"></asp:requiredfieldvalidator>
        </p>
        <p>
            <asp:label id="userSurnameLabel"
                text="Surname:"
                associatedcontrolid="userSurnameTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userSurnameTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="surnameRequiredFieldValidator" runat="server"
                controltovalidate="userSurnameTextBox" display="Dynamic"
                errormessage="Please enter your surname" forecolor="Red"></asp:requiredfieldvalidator>
        </p>
        <p>
            <asp:label id="userEmailLabel"
                text="Email:"
                associatedcontrolid="userEmailTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userEmailTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="emailRequiredFieldValidator" runat="server"
                controltovalidate="userEmailTextBox" display="Dynamic"
                errormessage="Please enter your email" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:regularexpressionvalidator id="emailRegularExpressionValidator"
                runat="server" controltovalidate="userEmailTextBox" display="Dynamic"
                errormessage="Please enter an e-mail address in a valid format" forecolor="Red"
                validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
        </p>
        <p>
            <asp:label id="userPhoneLabel"
                text="Phone:"
                associatedcontrolid="userPhoneTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userPhoneTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="phoneRequiredFieldValidator" runat="server"
                controltovalidate="userPhoneTextBox" display="Dynamic"
                errormessage="Please enter your phone" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:regularexpressionvalidator id="phoneRegularExpressionValidator"
                runat="server" controltovalidate="userPhoneTextBox" display="Dynamic"
                errormessage="Please enter a phone number in a valid format" forecolor="Red"
                validationexpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:regularexpressionvalidator>
        </p>
        <p>
            <asp:label id="userMonthLabel"
                text="Month:"
                associatedcontrolid="userMonthList"
                runat="server">
            </asp:label>
            <asp:dropdownlist id="userMonthList" runat="server">
            <asp:ListItem>Styczeń</asp:ListItem>
             <asp:ListItem>Luty</asp:ListItem>
             <asp:ListItem>Marzec</asp:ListItem>
             <asp:ListItem>Kwiecień</asp:ListItem>
             <asp:ListItem>Maj</asp:ListItem>
             <asp:ListItem>Czerwiec</asp:ListItem>
             <asp:ListItem>Lipiec</asp:ListItem>
             <asp:ListItem>Sierpień</asp:ListItem>
             <asp:ListItem>Wrzesień</asp:ListItem>
             <asp:ListItem>Październik</asp:ListItem>
             <asp:ListItem>Listopad</asp:ListItem>
             <asp:ListItem>Grudzień</asp:ListItem>
        </asp:dropdownlist>
            <br />
            <asp:requiredfieldvalidator id="monthRequiredFieldValidator" runat="server"
                controltovalidate="userMonthList" display="Dynamic"
                errormessage="Please enter your birth month" forecolor="Red"></asp:requiredfieldvalidator>
        </p>
        <p>
            <asp:label id="userDateLabel"
                text="Birth:"
                associatedcontrolid="userDate"
                runat="server">
            </asp:label>
            <asp:textbox id="userDate" runat="server" />
            <asp:rangevalidator runat="server" id="rngDate"
                controltovalidate="userDate" type="Date"
                minimumvalue="01-01-1900" maximumvalue="31-12-2010"
                errormessage="Please enter a valid date within 1900-2010" />
        </p>
        <p>
            <asp:label id="smallerNumberLabel"
                text="Smaller number:"
                associatedcontrolid="smallerNumberTextBox"
                runat="server">
            </asp:label>
            <asp:textbox runat="server" id="smallerNumberTextBox" type="number" />
            <br />
            <asp:requiredfieldvalidator id="smallerNumRequired" runat="server"
                controltovalidate="smallerNumberTextBox" display="Dynamic"
                errormessage="Please enter number" forecolor="Red"></asp:requiredfieldvalidator>
        </p>
        <p>
            <asp:label id="biggerNumberLabel"
                text="Bigger number:"
                associatedcontrolid="biggerNumberTextBox"
                runat="server">
            </asp:label>
            <asp:textbox runat="server" id="biggerNumberTextBox" type="number" />
            <br />
            <asp:requiredfieldvalidator id="biggerNumRequired" runat="server"
                controltovalidate="biggerNumberTextBox" display="Dynamic"
                errormessage="Please enter number" forecolor="Red"></asp:requiredfieldvalidator>
        </p>
        <p>
            <asp:comparevalidator runat="server"
                id="cmpNumbers"
                controltovalidate="biggerNumberTextBox"
                controltocompare="smallerNumberTextBox"
                operator="GreaterThan"
                type="Integer"
                errormessage="The first number should be smaller than the second number!" />
        </p>
        <p>
            <asp:button id="submitButton" runat="server" text="Submit" />
        </p>
    </form>

    <p>
        <asp:label id="outputLabel" runat="server" visible="False"></asp:label>
    </p>

    <p><a class="goToMain" href="#" onclick="goToMainAction();">Powrót do strony głownej</a></p>



    <asp:HyperLink ID="Link1" runat="server" NavigateUrl="About.aspx" Text="About" />

    <footer>
        <h6>&copy; 2018 by Gogola Piotr<wbr>
            All Rights Reserved.</h6>
        <address>
            Contact us at <a href="mailto:piotr.gogola@interia.pl">Gogola Piotr e-mail</a>
        </address>
    </footer>
</body>
</html>
