﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShoppingSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null != Session["final_cost"] && null != Session["order_way"])
        {
            order_cost.Text = Session["final_cost"].ToString();
            order_way.Text = Session["order_way"].ToString();
        }
        Session.Clear();
    }
}