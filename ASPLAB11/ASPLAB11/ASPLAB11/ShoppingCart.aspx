﻿<%@ page language="C#" autoeventwireup="true" codefile="ShoppingCart.aspx.cs" inherits="ShoppingCart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="aspnet.css">
    <title></title>

    <script type="text/javascript">

    </script>

</head>
<body>
    <div>
        <h1>Finalizacja transakcji
        </h1>
    </div>
    <form id="shoppingCartForm" runat="server">
        <div>
            <asp:radiobuttonlist id="delivery_option" runat="server" autopostback="True" onselectedindexchanged="UpdateFinalCost">
            <asp:ListItem ID="kurier" Selected="True">Kurier 15zł</asp:ListItem>
            <asp:ListItem ID="poczta">Poczta 10zł</asp:ListItem>
            <asp:ListItem ID="odbior_osobisty">Odbiór osobisty 0zł</asp:ListItem>
            </asp:radiobuttonlist>
        </div>
        <asp:label id="error_label" runat="server" visible="False" style="color:red;" text="Niepoprawne dane. Wprowadzona wartość musi być liczbą nieujemną"></asp:label>
        <div>
            Lista elementów w koszyku:
        <asp:label class="order" id="product_list" runat="server" visible="False"></asp:label>
            <asp:table id="product_table" runat="server" width="100%" visible="False"> 
            <asp:TableRow class="order" >
                <asp:TableCell class="order" >Nazwa produktu</asp:TableCell>
                <asp:TableCell class="order" >Cena jednostkowa</asp:TableCell>
                <asp:TableCell class="order" >Liczba zamówionych elementów</asp:TableCell>
                <asp:TableCell class="order" >Cena całościowa</asp:TableCell>
            </asp:TableRow>
        </asp:table>
        </div>
        <div>
            <asp:button id="clear_cart" runat="server" Text="Wyczyść koszyk" visible="True" OnClick="ButtonClearCart"></asp:button>
        </div>

        <div>
        <asp:label id="product_cost" runat="server" visible="True"></asp:label>
        </div>
        <div>
        <asp:label id="order_cost" runat="server" visible="True"></asp:label>
        </div>
        <div>
            <asp:hyperlink id="toProducts" navigateurl="Products.aspx" text="Powrót do listy produktów" runat="server" />
        </div>
        <div>
            <asp:hyperlink id="toSummary" navigateurl="ShoppingSummary.aspx" text="Zakończ zamówienie" runat="server" />
        </div>
    </form>

</body>
</html>
