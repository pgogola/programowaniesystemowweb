﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShoppingCart : System.Web.UI.Page
{
    private Dictionary<string, double> priceList = new Dictionary<string, double>()
    {
                {"18pF", 0.10},
                {"20pF", 0.12},
                {"33pF", 0.15},
                {"47pF", 0.25},
                {"0", 0.50},
                {"220", 0.30},
                {"1M", 0.33},
                {"100k", 0.29},
                {"47k", 0.47},
                {"STM32F100C4T6B", 14.56},
                {"STM32F100C8T6B", 20.00},
                {"STM32F100R8T6B", 9.00},
                {"STM32F091RCT6", 31.50},
                {"STM32L053R8T6", 19.99},
                {"1N4007", 0.50},
                {"MBR20100", 0.62},
                {"1N5408", 0.90},
                {"MUR160", 1.15}
    };

    private Dictionary<string, string> fullNames = new Dictionary<string, string>()
    {
                {"18pF", "Kondensator 18pF"},
                {"20pF", "Kondensator 20pF"},
                {"33pF", "Kondensator 33pF"},
                {"47pF", "Kondensator 47pF"},
                {"0", "Rezystor 0\u2126"},
                {"220", "Rezystor 220\u2126"},
                {"1M", "Rezystor 1M\u2126"},
                {"100k", "Rezystor 100k\u2126"},
                {"47k", "Rezystor 47k\u2126"},
                {"STM32F100C4T6B", "Mikrokontroler STM32F100C4T6B"},
                {"STM32F100C8T6B", "Mikrokontroler STM32F100C8T6B"},
                {"STM32F100R8T6B", "Mikrokontroler STM32F100R8T6B"},
                {"STM32F091RCT6", "Mikrokontroler STM32F091RCT6"},
                {"STM32L053R8T6", "Mikrokontroler STM32L053R8T6"},
                {"1N4007", "Dioda 1N4007"},
                {"MBR20100", "Dioda MBR20100"},
                {"1N5408", "Dioda 1N5408"},
                {"MUR160", "Dioda MUR160"}
    };

    private double productPriceSum = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Session["amount_products"] || 0 == (int)Session["amount_products"])
        {
            product_list.Text = "Koszyk jest pusty!";
            product_table.Visible = false;
            product_list.Visible = true;
            product_cost.Visible = false;
            order_cost.Visible = false;
            clear_cart.Visible = false;
            toSummary.Visible = false;
            delivery_option.Visible = false;
        }
        else
        {
            foreach (KeyValuePair<string, int> entry in (Dictionary<string, int>)Session["product_list"])
            {
                System.Diagnostics.Debug.WriteLine("Key=" + entry.Key + " Value=" + entry.Value);
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Text = fullNames[entry.Key]; 
                row.Cells.Add(cell1);

                TableCell cell2 = new TableCell();
                cell2.Text = priceList[entry.Key].ToString();
                row.Cells.Add(cell2);

                TableCell cell3 = new TableCell();
                TextBox textBox = new TextBox();
                textBox.Text = entry.Value.ToString();
                textBox.ID = entry.Key;
                textBox.AutoPostBack = true;

                textBox.TextChanged += UpdateProductAmount;
                //textBox.AutoPostBack = true;
                cell3.Controls.Add(textBox); //Text = entry.Value.ToString();
                row.Cells.Add(cell3);
                TableCell cell4 = new TableCell();
                cell4.ID = entry.Key + "fp";
                cell4.Text = (priceList[entry.Key] * entry.Value).ToString();
                row.Cells.Add(cell4);

                product_table.Rows.Add(row);
            }
            calculateProductSumPrice();
            System.Diagnostics.Debug.WriteLine(delivery_option.SelectedItem.Attributes["ID"]);

            product_cost.Text = "Koszt produktów: " + productPriceSum.ToString();
            order_cost.Text = "Łączny koszt zamówienia: " + (productPriceSum + getDeliveryCost()).ToString();
            Session.Add("final_cost", productPriceSum + getDeliveryCost());
            product_table.Visible = true;
            product_list.Visible = false;
            product_cost.Visible = true;
            order_cost.Visible = true;
            clear_cart.Visible = true;
            toSummary.Visible = true;
            delivery_option.Visible = true;
        }
    }

    private void calculateProductSumPrice()
    {
        productPriceSum = 0;
        foreach (KeyValuePair<string, int> entry in (Dictionary<string, int>)Session["product_list"])
        {
            productPriceSum += priceList[entry.Key] * entry.Value;
        }
        product_cost.Text = "Koszt produktów: " + productPriceSum.ToString();
    }

    private double getDeliveryCost()
    {
        switch (delivery_option.SelectedItem.Attributes["ID"])
        {
            case "kurier":
                Session.Add("order_way", "Kurier");
                return 15.00;
            case "poczta":
                Session.Add("order_way", "Poczta");
                return 10.00;
            case "odbior_osobisty":
                Session.Add("order_way", "Odbiór osobisty");
                return 0.00;
        }
        return 0;
    }

    protected void UpdateFinalCost(object sender, EventArgs e)
    {
        Session.Add("final_cost", productPriceSum + getDeliveryCost());
        order_cost.Text = "Łączny koszt zamówienia: " + (productPriceSum + getDeliveryCost()).ToString();
    }

    protected void UpdateProductAmount(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("UpdateProductAmount");
        System.Diagnostics.Debug.WriteLine(sender.ToString());
        TextBox tb = (TextBox)sender;
        try
        {
            if (tb.Text == null || 0 > int.Parse(tb.Text))
            {
                tb.Text = 0.ToString();
                tb.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                tb.BackColor = System.Drawing.Color.White;
                error_label.Visible = false;
            }
        }catch(Exception ex)
        {
            tb.Text = 0.ToString();
            tb.BackColor = System.Drawing.Color.Red;
            error_label.Visible = true;
        }

        TableCell tc = (TableCell)FindControl(tb.ID + "fp");
        ((Dictionary<string, int>)Session["product_list"])[tb.ID] = (int.Parse(tb.Text));

        int productsInShoppingCart = 0;
        foreach (KeyValuePair<string, int> entry in (Dictionary<string, int>)Session["product_list"])
        {
            productsInShoppingCart += entry.Value;
        }

        Session["amount_products"] = productsInShoppingCart;

        tc.Text = (Double.Parse(tb.Text) * Double.Parse(priceList[tb.ID].ToString())).ToString();
        calculateProductSumPrice();
        UpdateFinalCost(null, null);
    }

    protected void ButtonClearCart(object sender, EventArgs e)
    {
        Session["amount_products"] = 0;
        Session["product_list"] = new Dictionary<string, int>();
        product_list.Text = "Koszyk jest pusty!";
        for (int i = 1; i < product_table.Rows.Count; i++)
        {
            product_table.Rows.RemoveAt(i);
        }
        product_table.Visible = false;
        product_list.Visible = true;
        product_cost.Visible = false;
        order_cost.Visible = false;
        clear_cart.Visible = false;
        toSummary.Visible = false;
        delivery_option.Visible = false;
    }
}