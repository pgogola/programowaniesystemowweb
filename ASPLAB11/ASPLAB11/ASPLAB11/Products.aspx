﻿<%@ page language="C#" autoeventwireup="true" codefile="Products.aspx.cs" inherits="Products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="aspnet.css">
    <title></title>
</head>
<body>
    <div>
        <h1>Lista elementów elektronicznych
        </h1>
    </div>
    <form id="form1" runat="server">
        <div>
            <h2>Kategorie produktów</h2>
            <asp:radiobuttonlist id="product_types" runat="server" autopostback="True" onselectedindexchanged="RadBtnProductType_Click">
            <asp:ListItem ID="kondensatory">Kondensatory</asp:ListItem>
            <asp:ListItem ID="rezystory">Rezystory</asp:ListItem>
            <asp:ListItem ID="mikrokontrolery">Mikrokontrolery</asp:ListItem>
            <asp:ListItem ID="polprzewodniki">Półprzewodniki</asp:ListItem>
         </asp:radiobuttonlist>
        </div>
        <div>
            <h3>Dostępne produkty wybranej kategorii</h3>
            <asp:checkboxlist id="products_list1" visible="False" runat="server">
                <asp:ListItem ID="pF18">18pF</asp:ListItem>
                <asp:ListItem ID="pF20">20pF</asp:ListItem>
                <asp:ListItem ID="pF33">33pF</asp:ListItem>
                <asp:ListItem ID="pF47">47pF</asp:ListItem>
                <asp:ListItem ID="nF1">1nF</asp:ListItem>
            </asp:checkboxlist>
            <asp:checkboxlist id="products_list2" visible="False" runat="server">
                <asp:ListItem ID="R0">0</asp:ListItem>
                <asp:ListItem ID="R220">220</asp:ListItem>
                <asp:ListItem ID="R1M">1M</asp:ListItem>
                <asp:ListItem ID="R100k">100k</asp:ListItem>
                <asp:ListItem ID="R47k">47k</asp:ListItem>
            </asp:checkboxlist>
            <asp:checkboxlist id="products_list3" visible="False" runat="server">
                <asp:ListItem ID="STM32F100C4T6B">STM32F100C4T6B</asp:ListItem>
                <asp:ListItem ID="STM32F100C8T6B">STM32F100C8T6B</asp:ListItem>
                <asp:ListItem ID="STM32F100R8T6B">STM32F100R8T6B</asp:ListItem>
                <asp:ListItem ID="STM32F091RCT6">STM32F091RCT6</asp:ListItem>
                <asp:ListItem ID="STM32L053R8T6">STM32L053R8T6</asp:ListItem>
            </asp:checkboxlist>
            <asp:checkboxlist id="products_list4" visible="False" runat="server">
                <asp:ListItem ID="DIODA1N4007">1N4007</asp:ListItem>
                <asp:ListItem ID="MBR20100">MBR20100</asp:ListItem>
                <asp:ListItem ID="DIODA1N5408">1N5408</asp:ListItem>
                <asp:ListItem ID="MUR160">MUR160</asp:ListItem>
            </asp:checkboxlist>
        </div>
        <div>
            <asp:button id="add_product_button" text="Dodaj produkty" runat="server" onclick="BtnAddProduct_Click" />
        </div>
    </form>
    <div>
        Liczba elementów w koszyku:
        <asp:label id="product_amount" runat="server" visible="True"></asp:label>
    </div>
    <div>
        <asp:hyperlink id="toShoppingCart" navigateurl="ShoppingCart.aspx" text="Koszyk" runat="server">
            <asp:Image ID = "Image1" runat = "server" style = "width:30px;height:30px;" ImageUrl="cart.png"/>

        </asp:hyperlink>
    </div>

</body>
</html>
