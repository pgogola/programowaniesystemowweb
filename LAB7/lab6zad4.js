

var arraySize = ["10pt", "12pt", "14pt", "16pt", "18pt", "20pt"];
var i = 0;
function changeColor( e ){
    console.log("kliknieta");
    
    if( e.target.tagName.toLowerCase() == "p")
    {
        if ( e.ctrlKey ){
            e.target.style.color = "green";
        }
        else if( e.shiftKey ){
            e.target.style.color = "blue";
        }
        else if( e.altKey ){
            e.target.style.fontSize = arraySize[i];
            i++;
            i = i%arraySize.length;
            e.target.style.color = "black";
        }
        else if( e.keyCode == 78){
            e.target.innerHtml = "Nowy tekst";
        }
    }
}

function keyboardEvent( e ){
    console.log(e.keyCode);
    document.getElementById("code").innerHTML=e.keyCode;
}

function mouseMove ( e ){
    var x = e.screenX;
    var y = e.screenY;
    
    var r = 0;
    var g = Math.floor(x*255/screen.height);
    var b = Math.floor(y*255/screen.width);


    var rgbToHex = function(r, g, b){
        rH = Number(r).toString(16);
        gH = Number(g).toString(16);
        bH = Number(b).toString(16);
        console.log(rH+gH+bH);

        return "#"+rH+gH+bH;
    }

    document.getElementById("colorTest").style.backgroundColor = rgbToHex(r, g, b);

}

function start()
{
    document.body.addEventListener( "mousemove", mouseMove, false);
}
window.addEventListener( "load", start, false );


window.addEventListener( "click", changeColor, false );
window.addEventListener( "keydown", keyboardEvent, false );
