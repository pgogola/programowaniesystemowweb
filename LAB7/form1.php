<!DOCTYPE html>

<html>
   <head>
      <meta charset = "utf-8">
      <title>Informacja z formularza 1</title>
      <link rel="stylesheet" type="text/css" href="index.css">
   </head>
   <body>
      <?php
         print("<p>Uruchomiony skrypt to: {$_SERVER["SCRIPT_FILENAME"]}</p>");
         define("PHONE_REGEX", "/^(\([[:digit:]]{3}\) )?[[:digit:]]{3}-[[:digit:]]{3}-[[:digit:]]{3}$/");
         define("EMAIL_REGEX", "/^([[:alnum:]]+)@([[:alnum:]]+)\.(pl|com|eu|com\.pl)$/");

         $name=$_POST["userName"];
         $surname=$_POST["userSurname"];
         $month=$_POST["userMonth"];
         $email=$_POST["userEmail"];
         $tel=$_POST["userTel"];

         if(empty($name) or empty($surname))
         {
            die("<p>Nie podano imienia lub nazwiska</p></body></html>");
         }

         print("<p>Witaj " . $name . " " . $surname . "!<p>");

         if(!preg_match(PHONE_REGEX, $tel))
         {
            print("<p>Niepoprawny numer telefonu.<br>".
                  "Numer powinien odpowiadać postaci: (555) 555-555-555</p>");
         }
         else
         {
            print("<p>Twój numer telefonu to: ". $tel ."</p>");
         }
         
         if(!preg_match(EMAIL_REGEX, $email))
         {
            print("<p>Niepoprawna forma adresu e-mail.<br>".
                  "Adres e-mail powinien być postaci: ".
                  "([[:alnum:]]+)@([[:alnum:]]+)\.(pl|com|eu|com\.pl)</p>");
         }
         else
         {
            print("<p>Twój adres e-mail to: ". $email ."</p>");
         }
      ?>

      <p><a class="goToMain" href="form1.html">Powrót</a></p>
   </body>
</html>