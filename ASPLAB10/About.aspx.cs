﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            outputLabel.Text = "Witryna zrealizowana w technologii ASP.NET w ramach kursu \"Programowanie systemów webowych\"<br/>";
            outputLabel.Text += "Autor: Gogola Piotr<br/><br/>";
            outputLabel.Text += "Aktualny czas serwera: " + DateTime.Now.ToString();
            outputLabel.Visible = true;
            showInfo.Visible = false;
            label.Visible = false;
        }
    }
}