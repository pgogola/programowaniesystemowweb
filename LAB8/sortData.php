<!DOCTYPE html>

<html>
   <head>
      <meta charset = "utf-8">
      <title>Informacja z formularza 1</title>
      <link rel="stylesheet" type="text/css" href="index.css">
   </head>
   <body>
        <?php
        print("<p>Uruchomiony skrypt to: {$_SERVER["SCRIPT_FILENAME"]}</p>");
            function bubbleSort($array){
            $len = count($array) - 1;
            do{
                for($i=0;$i<len;++$i){
                    if($array[$i] > $array[$i+1])
                    {
                        $tmp = $array[$i];
                        $array[$i] = $array[$i+1];
                        $array[$i+1] = $tmp;
                    }
                }
                $len -= 1;
            }while($len>1);
        };
        $processedComment = "";
        $dataToSort = $_POST["data_to_sort"];
        while(preg_match("/(([[:alnum:]]+) *-> *([[:alnum:]]+))/", $dataToSort, $match)){
            $dataToSort = preg_replace("/".$match[1]."/", "", $dataToSort);
            preg_match("/\b(([[:alnum:]]+))\b/", $match[1], $key);
            $match[1] = preg_replace("/".$key[1]."/", "", $match[1], 1);
            preg_match("/\b(([[:alnum:]]+))\b/", $match[1], $value);
            $arr[$key[1]] = $value[1];
        }

        print("<p><div>Wprowadzone dane:<div>");
        foreach ( $arr as $key => $value ){
            print( "<div>Klucz: {$key} wartość: {$value}</div>" );
        }
        print("</p><p><div>Dane posortowane według klucza:<div>");
        ksort($arr);
        for(reset($arr); $i = key($arr); next($arr)){
            print("<div>Klucz: $i wartość $arr[$i]</div>" );
        }
        print("</p>");

        print("<p><div>Dane posortowane według wartości:<div>");
        asort($arr);
        for(reset($arr); $i = key($arr); next($arr)){
            print("<div>Klucz: $i wartość $arr[$i]</div>" );
        }
        print("</p>");
        
        $op1 = $_POST["op1"];
        $op2 = $_POST["op2"];

        print("<p>");
        print("<div>Przesłano dodatkowe ukryte informacje</div>");

        print("<div>1. '$op1' jest typu " . gettype($op1) . "</div>");
        $op1Int = (int) $op1;
        print("<div>1a. Po przekonwertowaniu '$op1Int' jest typu  " . gettype($op1Int) . "</div>");
        $op1Float = (float) $op1;
        print("<div>1b. Po przekonwertowaniu '$op1Float' jest typu  " . gettype($op1Float) . "</div>");

        print("<div>2. '$op2' jest typu " . gettype($op2) . "</div>");
        $op2Int = $op2;
        settype($op2Int, "integer");
        print("<div>2a. Po przekonwertowaniu '$op2Int' jest typu  " . gettype($op2Int) . "</div>");
        $op2Float = $op2;
        settype($op2Float, "float");
        print("<div>2b. Po przekonwertowaniu '$op2Float' jest typu  " . gettype($op2Float) . "</div>");
        print("</p>");

        print("<p><div>Porównując obydwie dane jako string</div>");
        if($op1<$op2){
            print("<div>{$op1} jest w porządku alfabetyczym przed {$op2}</div></p>");
        }
        elseif($op1>$op2){
            print("<div>{$op2} jest w porządku alfabetyczym przed {$op1}</div></p>");
        }
        if($op1==$op2){
            print("<div>{$op1} jest w równe {$op2}</div></p>");
        }
        print("<p><div>Porównując obydwie dane jako liczby int</div>");
        if($op1Int<$op2Int){
            print("<div>{$op1Int} jest mniejsze niż {$op2Int}</div></p>");
        }
        elseif($op1Int>$op2Int){
            print("<div>{$op2Int} jest mniejsze niż {$op1Int}</div></p>");
        }
        if($op1Int==$op2Int){
            print("<div>{$op1Int} jest równe {$op2Int}</div></p>");
        }

        $dodatkowo = "2 zlote";
        $wynik = $op1Int + $op2Int + $dodatkowo;

        print("<p><div>Dodajmy do tych danych $dodatkowo, a otrzymamy: ".
                $wynik ."</div></p>");
      ?>

      <p><a class="goToMain" href="generator.html">Powrót</a></p>
   </body>
</html>