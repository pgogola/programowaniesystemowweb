var helpArray =
    ["Wprowadź swoje imię",
        "Wprowadź swoje nazwisko",
        "Wprowadź miesiąc urodzenia",
        "Identyfikator_Użytkownika@Domena.com/pl",
        "###-###-###",
        ""]

function listenerAdd(id, idx) {
    document.getElementById(id)
        .addEventListener("focus", function () {
            document.getElementById("helper").innerHTML = helpArray[idx];
        });
    document.getElementById(id)
        .addEventListener("blur", function () {
            document.getElementById("helper").innerHTML = helpArray[5];
        });
}

function start() {
    listenerAdd("userName", 0);
    listenerAdd("userSurname", 1);
    listenerAdd("userMonth", 2);
    listenerAdd("userEmail", 3);
    listenerAdd("userTel", 4);
}

window.addEventListener("load", start, false);
