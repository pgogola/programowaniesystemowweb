<?php
    define( "FIVE_DAYS",  24 * 60 * 60 * 5 );//

    $backgroundColor = isset($_POST["bg_color"]) ? $_POST["bg_color"] : $_COOKIE["bg_color"];
    $textColor = isset($_POST["text_color"]) ? $_POST["text_color"] : $_COOKIE["text_color"];
    $textStyle = isset($_POST["font_style"]) ? $_POST["font_style"] : $_COOKIE["font_style"];

    setcookie( "bg_color", $backgroundColor, time() + FIVE_DAYS );
    setcookie( "text_color", $textColor, time() + FIVE_DAYS );
    setcookie( "font_style", $textStyle, time() + FIVE_DAYS );
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Zapisane ustawienia strony</title>
</head>
    <body style="background-color:<?php print( $backgroundColor )?>;">
        <p>Zapisano poniższe ustawienia strony</p>
        <p>Kolor tła: <?php print( $backgroundColor )?></p>
        <p>Kolor czcionki: 
            <span style = "color: <?php print( $textColor ) ?>;">
                <?php print( $textColor  )?>
            </span>
        </p>
        <p>Styl czcionki: 
            <span style = "font-family: <?php print( $textStyle ) ?>;">
                <?php print( $textStyle ) ?>
            </span>
        </p>
    </body>
</html>