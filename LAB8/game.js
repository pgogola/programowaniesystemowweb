var questions = ["Który typ w C++ jest typem zmiennopozycyjnym podwójnej precyzji?",
    "Jaki znak realizuje dereferencję wskaźnika?",
    "Jaki znak oznacza parametr funkcji jako referencję?",
    "Jakie słowo kluczowe umożliwia automatyczną dedukcję typu w C++?"]

var answers = []

function Question(question, answers, correctA) {
    this.question = question;
    this.answers = answers;
    this.correctAnswer = correctA;
}
var q1 = new Question("Który typ w C++ jest typem zmiennopozycyjnym podwójnej precyzji?",
    ["double", "int", "long long", "float"], 0);
var q2 = new Question("Jaki znak realizuje dereferencję wskaźnika?",
    ["&", "*", "!", "//"], 1)
var q3 = new Question("Jaki znak oznacza parametr funkcji jako referencję?",
    ["&", "*", "!", "//"], 0)
var q4 = new Question("Jakie słowo kluczowe umożliwia automatyczną dedukcję typu w C++?",
    ["Nie ma takiego", "dectype", "var", "auto"], 3);

var questions = [q1, q2, q3, q4];


var questionLabel;
var answersRadioButton;
var answersRadioLabel;
var scores;
var correctAnswersTab;
var acceptButton;
var currentQuestionIdx;
var scoresTab;

function start() {
    currentQuestionIdx = 0;
    questionLabel = document.getElementById("pytanie");
    answersRadioButton = [document.getElementById("one"),
    document.getElementById("two"),
    document.getElementById("three"),
    document.getElementById("four")];
    answersRadioLabel = [document.getElementById("one_label"),
    document.getElementById("two_label"),
    document.getElementById("three_label"),
    document.getElementById("four_label")];
    acceptButton = document.getElementById("acceptButton");
    acceptButton.addEventListener("click", onAcceptButtonListener, false);
    scoresTab = document.getElementById("scoresTab");
    scores = 0;
    correctAnswersTab = [];
    loadQuestion();
}

function loadQuestion() {
    questionLabel.innerHTML = questions[currentQuestionIdx].question;
    i = 0;
    while(i < 4) {
        answersRadioLabel[i].innerHTML = questions[currentQuestionIdx].answers[i];
        i++;
    }
}

function onAcceptButtonListener() {
    if (correctAnswersTab[currentQuestionIdx] = answersRadioButton[questions[currentQuestionIdx].correctAnswer].checked) {
        scores++;
    }
    currentQuestionIdx++;
    scoresTab.innerHTML = createTable();
    if (currentQuestionIdx > 3) {
        showSummary();
    }
    else {
        loadQuestion(currentQuestionIdx)
    }
}

function createTable() {
    var table = new String("<table>" +
        "<caption>Wyniki dotychczasowych odpowiedzi</caption>" +
        "<thead><th>Nr. pytania</th><th>Czy poprawnie?</th><th>Poprawna odpowiedź</th></thead>" +
        "<tbody>");
    for (i = 0; i < currentQuestionIdx; i++) {
        table = table + "<tr><td>" + (i + 1) + "</td><td>" +
            (correctAnswersTab[i] ? "TAK" : "NIE") + "</td><td>" +
            questions[i].answers[questions[i].correctAnswer] + "</td></tr>";
    }
    return table;
}

function showSummary() {
    acceptButton.style.display = 'none';
    i = 0;
    do{
        answersRadioLabel[i].style.display = 'none';
        answersRadioButton[i].style.display = 'none';
        i++;
    }while(i<4)
    document.getElementById("header_pytanie").style.display = 'none';
    questionLabel.innerHTML = "Twój wynik: " + scores + ".";
    switch (scores) {
        case 0:
            questionLabel.innerHTML += " Poćwicz trochę.";
            break;
        case 1:
            questionLabel.innerHTML += " Widać postępy!";
            break;
        case 2:
            questionLabel.innerHTML += " Jeszcze trochę, a będziesz mistrzem.";
            break;
        case 3:
            questionLabel.innerHTML += " Brakuje Ci oooo __ tyle.";
            break;
        case 4:
            questionLabel.innerHTML += " Jesteś mistrzem C++.";
            break;

    }
}

window.addEventListener("load", start, false);
