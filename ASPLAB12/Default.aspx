﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Strona główna
    </h2>
    <p>
        Poniżej możesz przeglądać listę dostępnych produktów. W celu wykonywania dodatkowych operacji musisz być zalogowanym użytkownikiem.
    </p>

    <p>
      Typ produktów:
      <asp:DropDownList ID="productsTypeDropDownList" runat="server" AutoPostBack="True" 
         DataSourceID="productsTypeLinqDataSource" DataTextField="ProductTypeName" 
         DataValueField="ProductTypeID" 
         onselectedindexchanged="productsTypeDropDownList_SelectedIndexChanged">
      </asp:DropDownList>
      <asp:LinqDataSource ID="productsTypeLinqDataSource" runat="server" 
         onselecting="productsTypeLinqDataSource_Selecting">
      </asp:LinqDataSource>
      <asp:GridView ID="productsGridView" runat="server" CSSClass="table_data"
         AllowSorting="True" AutoGenerateColumns="False" 
         DataSourceID="productsLinqDataSource" Width="580px">
         <Columns>
            <asp:BoundField DataField="product_name" HeaderText="Nazwa produktu" SortExpression="product_name" />
            <asp:BoundField DataField="price" HeaderText="Cena [zł]" SortExpression="price" />
            <asp:BoundField DataField="elements_amount" HeaderText="Liczba elementów" SortExpression="elements_amount" />
         </Columns>
      </asp:GridView>
      <asp:LinqDataSource ID="productsLinqDataSource" runat="server" 
         onselecting="productsLinqDataSource_Selecting">
      </asp:LinqDataSource>
   </p>

</asp:Content>
