﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            Validate();

            if (IsValid)
            {
                string name = userNameTextBox.Text;
                string surname = userSurnameTextBox.Text;
                string email = userEmailTextBox.Text;
                string phone = userPhoneTextBox.Text;
                string userMonth = userMonthList.SelectedItem.Value;
                string date = userDate.Text;
                long smallerNum = Convert.ToInt64(smallerNumberTextBox.Text);
                long biggerNum = Convert.ToInt64(biggerNumberTextBox.Text);

                outputLabel.Text = "Dane przesłane na serwer:<br/>";
                outputLabel.Text +=
                   String.Format("Imię: {1}{0}Nazwisko: {2}{0}E-mail: {3}{0}Telefon: {4}{0}Miesiąć: {5}{0}Rok urodzenia: {6}{0}Liczby: {7} i {8}",
                      "<br/>", name, surname, email, phone, userMonth, date, smallerNum, biggerNum);

                long sum = smallerNum + biggerNum;
                long sub = biggerNum - smallerNum;
                long mod = biggerNum % smallerNum;
                outputLabel.Text +=
                    String.Format("{0}{0}Przykładowe wyniki operacji: {1} + {2} = {3}{0}{1} - {2} = {4}{0}{1} % {2} = {5}",
                    "<br/>", biggerNum, smallerNum, sum, sub, mod);


                outputLabel.Visible = true;
            }
        }
    }
}