﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Protected_EditDatabase : System.Web.UI.Page
{
    ProductsDataContext products = new ProductsDataContext();

    protected void productsTypeLinqDataSource_Selecting(object sender,
       LinqDataSourceSelectEventArgs e)
    {
        e.Result =
           from productType in products.ProductTypes
           select //productType.product_type_name;
            new
            {
                ProductTypeName = productType.product_type_name,
                ProductTypeID = productType.Id
            };
    }

    protected void productsLinqDataSource_Selecting(object sender,
       LinqDataSourceSelectEventArgs e)
    {
        e.Result =
           from product in products.Products
           where product.fk_product_type_name ==
              Convert.ToInt32(productsTypeDropDownList.SelectedValue)
           select product;
    }

    protected void productsLinqDataSource_Updating(object sender,
       LinqDataSourceUpdateEventArgs e)
    {
            Products originalProduct = (Products)e.OriginalObject;
            Products newProduct = (Products)e.NewObject;

            
            System.Diagnostics.Debug.WriteLine("udpate");
     }

    protected void productsTypeDropDownList_SelectedIndexChanged(
       object sender, EventArgs e)
    {
        productsGridView.DataBind();
    }

    protected void productsLinqDataSource_Updated(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("udpated");
        try
        {
            products.SubmitChanges();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        productsGridView.DataBind();
    }

    protected void SubmitBtn_Click(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("Button click");
        ProductsDataContext products = new ProductsDataContext();
        Products ord = new Products
        {
            fk_product_type_name = 1,
            elements_amount = 50,
            price = 1.25,
            product_name = "Ciagnik"
        };

        // Add the new object to the Orders collection.
        products.Products.InsertOnSubmit(ord);

        // Submit the change to the database.
        try
        {
            products.SubmitChanges();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
    }
}