﻿<%@ page language="C#"  MasterPageFile="~/Site.master" autoeventwireup="true" codefile="Form1.aspx.cs" inherits="Form1" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>
        <asp:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
        </asp:toolkitscriptmanager>
        <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0"
            width="450px">
        <asp:TabPanel runat="server" HeaderText="Imię i nazwisko" ID="TabPanel1">
        <ContentTemplate>
        <p>
            <asp:label id="userNameLabel"
                text="Imię:"
                associatedcontrolid="userNameTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userNameTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="nameRequiredFieldValidator" runat="server"
                controltovalidate="userNameTextBox" display="None"
                errormessage="Wymagane imie" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="nameRequiredFieldValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="nameRequiredFieldValidator">
            </asp:ValidatorCalloutExtender>
        
        </p>
        <p>
            <asp:label id="userSurnameLabel"
                text="Nazwisko:"
                associatedcontrolid="userSurnameTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userSurnameTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="surnameRequiredFieldValidator" runat="server"
                controltovalidate="userSurnameTextBox" display="None"
                errormessage="Wymagane nazwisko" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="surnameRequiredFieldValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="surnameRequiredFieldValidator">
            </asp:ValidatorCalloutExtender>
        </p>
         </ContentTemplate>
         </asp:TabPanel>
          <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="Dane kontaktowe">
             <ContentTemplate>
        <p>
            <asp:label id="userEmailLabel"
                text="Email:"
                associatedcontrolid="userEmailTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userEmailTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="emailRequiredFieldValidator" runat="server"
                controltovalidate="userEmailTextBox" display="None"
                errormessage="Wymagany adres email" forecolor="Red"></asp:requiredfieldvalidator>
                        <asp:ValidatorCalloutExtender ID="emailRequiredFieldValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="emailRequiredFieldValidator">
            </asp:ValidatorCalloutExtender>
            <asp:regularexpressionvalidator id="emailRegularExpressionValidator"
                runat="server" controltovalidate="userEmailTextBox" display="None"
                errormessage="Wprowadź email w poprawnyum formacie" forecolor="Red"
                validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
            <asp:ValidatorCalloutExtender ID="emailRegularFieldValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="emailRegularExpressionValidator">
            </asp:ValidatorCalloutExtender>
        </p>
        <p>
            <asp:label id="userPhoneLabel"
                text="Numer telefonu:"
                associatedcontrolid="userPhoneTextBox"
                runat="server">
            </asp:label>
            <asp:textbox id="userPhoneTextBox" runat="server"></asp:textbox>
            <br />
            <asp:requiredfieldvalidator id="phoneRequiredFieldValidator" runat="server"
                controltovalidate="userPhoneTextBox" display="None"
                errormessage="Wypełnij pole numeru telefonu" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="phoneRequiredFieldValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="phoneRequiredFieldValidator">
            </asp:ValidatorCalloutExtender>
            <asp:regularexpressionvalidator id="phoneRegularExpressionValidator"
                runat="server" controltovalidate="userPhoneTextBox" display="None"
                errormessage="Wprowadzony numer telefony nie jest poprawnego formatu" forecolor="Red"
                validationexpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:regularexpressionvalidator>
            <asp:ValidatorCalloutExtender ID="phoneRegularExpressionValidator_VCE" 
                runat="server" Enabled="True" TargetControlID="phoneRegularExpressionValidator">
            </asp:ValidatorCalloutExtender>
        </p>
                  </ContentTemplate>
         </asp:TabPanel>
          <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="Inne">
             <ContentTemplate>
        <p>
            <asp:label id="userMonthLabel"
                text="Miesiąc:"
                associatedcontrolid="userMonthList"
                runat="server">
            </asp:label>
            <asp:dropdownlist id="userMonthList" runat="server">
            <asp:ListItem>Styczeń</asp:ListItem>
             <asp:ListItem>Luty</asp:ListItem>
             <asp:ListItem>Marzec</asp:ListItem>
             <asp:ListItem>Kwiecień</asp:ListItem>
             <asp:ListItem>Maj</asp:ListItem>
             <asp:ListItem>Czerwiec</asp:ListItem>
             <asp:ListItem>Lipiec</asp:ListItem>
             <asp:ListItem>Sierpień</asp:ListItem>
             <asp:ListItem>Wrzesień</asp:ListItem>
             <asp:ListItem>Październik</asp:ListItem>
             <asp:ListItem>Listopad</asp:ListItem>
             <asp:ListItem>Grudzień</asp:ListItem>
        </asp:dropdownlist>
            <br />
            <asp:requiredfieldvalidator id="monthRequiredFieldValidator" runat="server"
                controltovalidate="userMonthList" display="None"
                errormessage="Wprowadź miesiąc urodzenia" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="monthRequiredFieldValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="monthRequiredFieldValidator">
            </asp:ValidatorCalloutExtender>
        </p>
        <p>
            <asp:label id="userDateLabel"
                text="Data urodzenia:"
                associatedcontrolid="userDate"
                runat="server">
            </asp:label>
            <asp:textbox id="userDate" runat="server" />
            <asp:rangevalidator id="dateRangeValidator" runat="server"
                controltovalidate="userDate" type="Date" display="None"
                minimumvalue="01-01-1900" maximumvalue="31-12-2010"
                errormessage="Wprowadź datę pomiędzy 1900-2010" />
            <asp:ValidatorCalloutExtender ID="dateRangeValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="dateRangeValidator">
            </asp:ValidatorCalloutExtender>
        </p>
        <p>
            <asp:label id="smallerNumberLabel"
                text="Mniejsza liczba:"
                associatedcontrolid="smallerNumberTextBox"
                runat="server">
            </asp:label>
            <asp:textbox runat="server" id="smallerNumberTextBox" type="number" />
       <%--     <asp:requiredfieldvalidator id="smallerNumRequired" runat="server"
                controltovalidate="smallerNumberTextBox" display="Dynamic"
                errormessage="Wprowadź liczbę" forecolor="Red"></asp:requiredfieldvalidator>--%>
        <%--    <asp:ValidatorCalloutExtender ID="smallerNumRequired_VCE" 
                            runat="server" Enabled="True" TargetControlID="smallerNumRequired">
            </asp:ValidatorCalloutExtender>--%>
        </p>
        <p>
            <asp:label id="biggerNumberLabel"
                text="Większa liczba:"
                associatedcontrolid="biggerNumberTextBox"
                runat="server">
            </asp:label>
            <asp:textbox runat="server" id="biggerNumberTextBox" type="number" />
        <%--    <asp:requiredfieldvalidator id="biggerNumRequired" runat="server"
                controltovalidate="biggerNumberTextBox" display="None"
                errormessage="Wprowadź liczbę" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="biggerNumRequired_VCE" 
                            runat="server" Enabled="True" TargetControlID="biggerNumRequired">
            </asp:ValidatorCalloutExtender>--%>
        </p>
       <%-- <p>
            <asp:comparevalidator runat="server"
                id="cmpNumbers"
                controltovalidate="biggerNumberTextBox"
                controltocompare="smallerNumberTextBox"
                operator="GreaterThan"
                type="Integer"
                errormessage="Pierwsza liczba musi być mniejsza od drugiej" 
                display="None"/>
            <asp:ValidatorCalloutExtender ID="cmpNumbers_VCE" 
                            runat="server" Enabled="True" TargetControlID="cmpNumbers">
            </asp:ValidatorCalloutExtender>
        </p>--%>
        
       </ContentTemplate>
          </asp:TabPanel>
       </asp:tabcontainer>
        <asp:updatepanel id="UpdatePanel1" runat="server">
          <ContentTemplate>
             <p>
                <asp:button id="submitButton" runat="server" text="Wyślij" />
             </p>
             <p>
                <asp:label id="outputLabel" runat="server" visible="False"></asp:label>
             </p>
              </ContentTemplate>
          <Triggers>
             <asp:AsyncPostBackTrigger ControlID="submitButton" EventName="Click" />
          </Triggers>
       </asp:updatepanel>
    </p>
    </asp:Content>
    