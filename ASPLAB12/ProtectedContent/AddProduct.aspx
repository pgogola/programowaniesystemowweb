﻿<%@ page title="" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" codefile="AddProduct.aspx.cs" inherits="ProtectedContent_AddProduct" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="MainContent" runat="Server">
    <h2>
        Definiowanie nowych produktów
    </h2>
    <asp:Label ID="info_label" runat="server" visible="False" style="font-size:1.2em;font-weight:bold;color:blue;"/>
    <asp:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
        </asp:toolkitscriptmanager>
    <div>
        <p>
            Rodzaj produktu:
            <asp:DropDownList ID="productTypeDropDownList" runat="server" AutoPostBack="True" 
                DataSourceID="productTypeLinqDataSource" DataTextField="productTypeName" 
                DataValueField="productId" 
                onselectedindexchanged="productTypeDropDownList_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:LinqDataSource ID="productTypeLinqDataSource" runat="server" 
                onselecting="productTypeLinqDataSource_Selecting">
            </asp:LinqDataSource>
        </p>
        <p>
            Nazwa produktu: 
            <asp:TextBox id="product_name" runat="server"></asp:TextBox>
            <asp:requiredfieldvalidator id="product_nameRequiredFieldValidator" runat="server"
                controltovalidate="product_name" display="None"
                errormessage="Wymagana nazwa produktu" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="product_nameRequiredFieldValidator_VCE" 
                            runat="server" Enabled="True" TargetControlID="product_nameRequiredFieldValidator">
            </asp:ValidatorCalloutExtender>
        </p>
        <p>
            Cena jednostkowa: 
            <asp:TextBox id="price" runat="server" type=""></asp:TextBox>
            <asp:requiredfieldvalidator id="priceRequiredfieldvalidator" runat="server"
                controltovalidate="price" display="None"
                errormessage="Wymagana cena jednostkowa produktu" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="priceRequiredfieldvalidator_VCE" 
                runat="server" Enabled="True" TargetControlID="priceRequiredfieldvalidator">
            </asp:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator ID="priceRegularExpressionValidator" ControlToValidate="price" runat="server"
                ErrorMessage="Niepoprawny format danych" Display="None" ValidationExpression="[0-9]*\.[0-9]{2}">
            </asp:RegularExpressionValidator>
          <asp:ValidatorCalloutExtender ID="priceRegularExpressionValidator_VCE" 
                runat="server" Enabled="True" TargetControlID="priceRegularExpressionValidator">
            </asp:ValidatorCalloutExtender>
        </p>

        <p>
            Liczba elementów w magazynie: 
            <asp:TextBox id="element_amount" runat="server" type="number"></asp:TextBox>
            <asp:requiredfieldvalidator id="element_amountRequiredfieldvalidator" runat="server"
                controltovalidate="price" display="None"
                errormessage="Wymagana liczbe elementów w magazynie" forecolor="Red"></asp:requiredfieldvalidator>
            <asp:ValidatorCalloutExtender ID="element_amountRequiredfieldvalidator_VCE" 
                runat="server" Enabled="True" TargetControlID="element_amountRequiredfieldvalidator">
            </asp:ValidatorCalloutExtender>
            <asp:CompareValidator  ID="element_amountRangeValidator" ControlToValidate="element_amount" runat="server"
                ValueToCompare="0"  ErrorMessage="Wartość nie może być mniejsza od zera" Display="None"
                Operator="GreaterThanEqual">
            </asp:CompareValidator>
            <asp:ValidatorCalloutExtender ID="element_amountRangeValidator_VCE" 
                runat="server" Enabled="True" TargetControlID="element_amountRangeValidator">
            </asp:ValidatorCalloutExtender>
        </p>

         <p>
            <asp:button id="submitButton" runat="server" text="Dodaj" />
         </p>
    </div>    
</asp:content>

