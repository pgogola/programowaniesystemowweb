﻿<%@ page title="" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" codefile="EditDatabase.aspx.cs" inherits="Protected_EditDatabase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="MainContent" runat="Server">
    <h2>
        Zarządzanie produktami
    </h2>
    <p>
        Poniżej możesz zarządzać listą dostępnych w ofercie produktów.
    </p>

      Typ produktów:
      <asp:DropDownList ID="productsTypeDropDownList" runat="server" AutoPostBack="True" 
         DataSourceID="productsTypeLinqDataSource" DataTextField="ProductTypeName" 
         DataValueField="ProductTypeID" 
         onselectedindexchanged="productsTypeDropDownList_SelectedIndexChanged">
      </asp:DropDownList>
      <asp:LinqDataSource ID="productsTypeLinqDataSource" runat="server" 
         onselecting="productsTypeLinqDataSource_Selecting">
      </asp:LinqDataSource>
      <asp:GridView ID="productsGridView" runat="server" AllowPaging="True" CSSClass="table_data"
         AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id"
         DataSourceID="productsLinqDataSource" Width="580px">
         <Columns>
            <asp:BoundField ReadOnly="true" DataField="Id" HeaderText="Numer ID produktu" SortExpression="Id" />
            <asp:BoundField DataField="product_name" HeaderText="Nazwa produktu" SortExpression="product_name" />
            <asp:BoundField DataField="price" HeaderText="Cena [zł]" SortExpression="price" />
            <asp:BoundField DataField="elements_amount" HeaderText="Liczba elementów" SortExpression="elements_amount" />
            <asp:commandfield showeditbutton="true" edittext="Edytuj produkt" updatetext="Zapisz" 
                showdeletebutton="true" deletetext="Usuń produkt"
                canceltext="Anuluj" headertext="Operacje"/>
         </Columns>
      </asp:GridView>
      <asp:LinqDataSource ID="productsLinqDataSource" runat="server" EnableDelete="True" 
          ContextTypeName="ProductsDataContext" EnableUpdate="True"
         TableName="Products" onupdating="productsLinqDataSource_Updating" 
          onupdated="productsLinqDataSource_Updated"
         onselecting="productsLinqDataSource_Selecting">
      </asp:LinqDataSource>
   </p>
</asp:content>

