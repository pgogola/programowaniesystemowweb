﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ProtectedContent_AddProduct : System.Web.UI.Page
{

    ProductsDataContext products = new ProductsDataContext();



    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            Validate();

            if (IsValid)
            {
                Products prod = new Products
                {
                    fk_product_type_name = Convert.ToInt32(productTypeDropDownList.SelectedValue),
                    elements_amount = Convert.ToInt32(element_amount.Text.ToString()),
                    price = Convert.ToDouble(price.Text, new CultureInfo("en-US")),
                    product_name = Convert.ToString(product_name.Text)
                };
                
                products.Products.InsertOnSubmit(prod);
                
                try
                {
                    products.SubmitChanges();
                    info_label.Text = "Dodano nowy produkt " + product_name.Text.ToString();
                    info_label.Visible = true;
                    element_amount.Text = "";
                    price.Text = "";
                    product_name.Text = "";
                }
                catch (Exception ex)
                {
                    info_label.Text = "Nie dodano nowego produktu. Błąd " + ex.ToString();
                    info_label.Visible = true;
                    Console.WriteLine(ex);
                }
            }
        }
    }


    protected void productTypeLinqDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("productTypeLinqDataSource_Selecting");
        e.Result =
           from product in products.ProductTypes
           select new
           {
               productTypeName = product.product_type_name,
               productId = product.Id
           };
    }

    protected void productTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("productTypeDropDownList_SelectedIndexChanged");
    }
}