﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    //protected void Page_Load(object sender, EventArgs e)
    //{
    ProductsDataContext database = new ProductsDataContext();

    protected void productsTypeLinqDataSource_Selecting(object sender,
       LinqDataSourceSelectEventArgs e)
    {
        e.Result =
           from productType in database.ProductTypes
           select //productType.product_type_name;
            new
            {
                ProductTypeName = productType.product_type_name,
                ProductTypeID = productType.Id
            };
    }

    protected void productsLinqDataSource_Selecting(object sender,
       LinqDataSourceSelectEventArgs e)
    {
        e.Result =
           from product in database.Products
           where product.fk_product_type_name ==
              Convert.ToInt32(productsTypeDropDownList.SelectedValue)
           select product;
    }

    protected void productsTypeDropDownList_SelectedIndexChanged(
       object sender, EventArgs e)
    {
        productsGridView.DataBind();
    }
    //}
}
