DROP DATABASE IF EXISTS webpage_users;

CREATE DATABASE webpage_users;

USE webpage_users;

CREATE TABLE users
(
   ID INT NOT NULL AUTO_INCREMENT UNIQUE,
   Name VARCHAR(30) NOT NULL,
   Lastname VARCHAR(30) NOT NULL,
   Email VARCHAR(50) NOT NULL UNIQUE,
   Age INT,
   Login VARCHAR(30) NOT NULL PRIMARY KEY,
   Password VARCHAR(225) NOT NULL
);

INSERT INTO users (Name, Lastname, Email, Age, Login, Password) 
   VALUES ("Piotr", "Gogola", "piotr@email.pl", 22, "piotr", md5("password"));

INSERT INTO users (Name, Lastname, Email, Age, Login, Password) 
   VALUES ("Marek", "Rokicki", "marek@email.pl", 18, "marek", md5("passwordm"));

INSERT INTO users (Name, Lastname, Email, Age, Login, Password) 
   VALUES ("Anna", "Rys", "anna@email.pl", 30, "anna", md5("passworda"));

INSERT INTO users (Name, Lastname, Email, Age, Login, Password) 
   VALUES ("Robert", "Skoczylas", "robert@email.pl", 25, "robert", md5("passwordr"));

INSERT INTO users (Name, Lastname, Email, Age, Login, Password) 
   VALUES ("Malgorzata", "Kowalska", "malgorzata@email.pl", 20, "malgorzata", md5("passwordm"));