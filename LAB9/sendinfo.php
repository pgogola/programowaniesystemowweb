<!DOCTYPE html>

<html>
    <head>
        <meta charset = "utf-8">
        <title>Informacja z formularza 1</title>
        <link rel="stylesheet" type="text/css" href="index.css">
    </head>
    <body>
        <?php
            print("<p>Uruchomiony skrypt to: {$_SERVER["SCRIPT_FILENAME"]}</p>");
            $ip=$_SERVER['REMOTE_ADDR'];
            print("<p>{$ip}</p>");

            $comments = $_POST["comments"];

            $processedComment = "";
            while(preg_match("/([[:alnum:]]+)/", $comments, $match)){
                $comments = preg_replace("/".$match[1]."/", "", $comments);
                $strlen = strlen($match[1]);
                $word = $match[1];
                if($strlen>1)
                {
                    $word = $match[1][$strlen-1] . substr($match[1], 1, $strlen-2) . $match[1][0];
                }
                $processedComment = $processedComment . $word . " ";
            }

            print("<p>Czy przesłana przez Ciebie wiadomość to na pewno: \"{$processedComment}\"?</p>");
        ?>
        <p><a class="goToMain" href="sendInfo.html">Powrót do strony głownej</a></p>
    </body>
</html>