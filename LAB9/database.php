<!DOCTYPE html>
<html lang="pl-PL">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="database.css">
    </head>
    <body>
        <?php
            $filterArray = array(
                "ID" => "ID",
                "Name" => "Name",
                "Lastname" => "Last name",
                "PhoneNum" => "Phone number",
                "Email" => "E-mail",
                "Age" => "Age",
                "Login" => "Login",
                "Password" => "Password"
            );
            if ( !( $database = mysqli_connect( "localhost" , "root", null) ) )
            {
                die( "<p>Could not connect to database</p></body></html>" );
            }        
            if (!mysqli_select_db($database, "webpage_users" ))
            {
                die( "<p>Could not open webpage_users database</p></body></html>" );
            }
            if(isset($_POST["query_text"])){
                $query_where = $_POST["query_text"];
            }
            $query = "SELECT * FROM users;";
            if(!empty($query_where) && $_POST['query_where']!='Brak'){
                $query = "SELECT * FROM users WHERE ".$_POST['query_where']." = '".$query_where."';";
            }
            echo $query;
            if(!$queryResult = mysqli_query( $database,  $query)){
                echo mysqli_error($database);
            };
            mysqli_close($database);
        ?>

        <form name="database_query" method="post" action="http://localhost:80/phpLab9/database.php">
            <?php
                $where_c = false;
                foreach($filterArray as $key => $desc)
                {
                    print('<div><label>'.$desc.' <input name="filter[]" type="checkbox" value="'. $key . '"');
                    if(isset($_POST['filter']) && in_array($key, $_POST['filter'])){
                        print(" checked");
                    }
                    print('></label><div>');
                }
                foreach($filterArray as $key => $desc)
                {
                    print('<div><label>'.$desc.' <input name="query_where" type="radio" value="'. $key . '"');
                    if(isset($_POST['query_where']) && $_POST['query_where'] == $key){
                        print('checked="checked"');
                        $where_c = true;
                    }
                    print('></label><div>');
                }
                if($where_c==false)
                {
                    print('<div><label>Brak <input name="query_where" type="radio" value="Brak" checked="checked"</label></div>');
                }else{
                    print('<div><label>Brak <input name="query_where" type="radio" value="Brak"</label></div>');
                }
            ?>
            <div><label for="id_query_text">Zapytanie: </label>
            <input id="id_query_text" type="text" name="query_text"></div>
            <div><input type="submit" name="Filtruj" value="Filtruj"></div>
        </form>

        <table>
            <caption>Baza danych użytkowników</caption>
            <tr>
                <?php
                if(isset($_POST['filter'])){
                    foreach($_POST['filter'] as $selected){
                        print('<th>'. $filterArray[$selected].'</th>');
                    }
                }
                ?>
            </tr>
                <?php
                if(isset($_POST['filter'])){
                    while($row = mysqli_fetch_assoc($queryResult)){
                        print('<tr>');
                        foreach($_POST['filter'] as $selected){
                            print('<td>' . $row[$selected] . '</td>');
                        }
                        print('</tr>');
                    }
                }
                ?>
        </table>


        <p><a class="goToMain" href="index.php">Powrót do strony głownej</a></p>
    </body> 
</html>