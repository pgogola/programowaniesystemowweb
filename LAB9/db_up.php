<?php
    $query = "select * from users";
    
    if ( !( $database = mysqli_connect( "localhost" , "root", null) ) )
    {
        die( "<p>Could not connect to database</p>" );
    }
    $createDatabaseQuery = "CREATE DATABASE webpage_users;";
    $switchToDatabaseQuery = "USE webpage_users;";
    $createTableQuery = "CREATE TABLE users
                        (
                        ID INT NOT NULL AUTO_INCREMENT UNIQUE,
                        Name VARCHAR(30) NOT NULL,
                        Lastname VARCHAR(30) NOT NULL,
                        Email VARCHAR(50) NOT NULL,
                        PhoneNum VARCHAR(50) NOT NULL,
                        Age INT,
                        Login VARCHAR(30) NOT NULL PRIMARY KEY,
                        Password VARCHAR(225) NOT NULL
                        );";
   
    if ( mysqli_query( $database, $createDatabaseQuery) )
    {
        print( "<p>Database created!</p>" );
    }else   
    {
        print( "<p>Could not create database!</p>" );
        die( mysql_error() );
    }
    
    if ( mysqli_query( $database, $switchToDatabaseQuery) )
    {
        print( "<p>Database switched!</p>" );
    }else
    {
        print( "<p>Could not switch to database!</p>" );
        die( mysql_error() );
    }
    
    if ( mysqli_query( $database, $createTableQuery) )
    {
        print( "<p>User table created!</p>" );
    }
    else{
        print( "<p>Could not create user table!</p>" );
        die( mysql_error() );
    }

    //create test data
    $testUsers = array(
        '("Piotr", "Gogola", "piotr@email.pl", "(555) 555-555-555", 22, "piotr", md5("password"))',
        '("Marek", "Rokicki", "marek@email.pl", "(432) 383-843-122", 18, "marek", md5("passwordm"))',
        '("Anna", "Rys", "anna@email.pl", "925-545-543", 30, "anna", md5("passworda"))',
        '("Robert", "Skoczylas", "robert@email.pl", "534-345-123", 25, "robert", md5("passwordr"))',
        '("Malgorzata", "Kowalska", "malgorzata@email.pl", "(097) 839-473-444", 20, "malgorzata", md5("passwordm"))'
    );

    foreach($testUsers as $userData){
        mysqli_query( $database, "INSERT INTO users (Name, Lastname, Email, PhoneNum, Age, Login, Password) 
                                  VALUES " . $userData . ";");
    }

    mysqli_close($database);
?>