<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="pl-PL">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="sesja.css">
    </head>
    <body>
        <?php
            define("PHONE_REGEX", "/^(\([[:digit:]]{3}\) )?[[:digit:]]{3}-[[:digit:]]{3}-[[:digit:]]{3}$/");
            define("EMAIL_REGEX", "/^([[:alnum:]]+)@([[:alnum:]]+)\.(pl|com|eu|com\.pl)$/");
            define("AGE_REGEX", "/^[[:digit:]]+$/");

            $user_login = "";
            $user_name = "";
            $user_lastname = "";
            $user_email = "";
            $user_phone = "";
            $user_age = "";
            $user_pass = "";

            $isError = false;

            if(isset($_POST['data_update'])){
                $name = $_POST['user_name'];
                $lastname = $_POST['user_lastname'];
                $email = $_POST['user_email'];
                $phone = $_POST['user_phone'];
                $age = $_POST['user_age'];
                $login = $_POST['user_login'];
                $password = $_POST['user_pass'];
                
                
                if(empty($phone) || !preg_match(PHONE_REGEX, $phone)){
                    print( "<p>Niepoprawny numer telefonu</p>" );
                    $isError = true;
                }
                if(empty($email) || !preg_match(EMAIL_REGEX, $email)){
                    print( "<p>Niepoprawny adres email</p>" );
                    $isError = true;
                }
                if(empty($age) || !preg_match(AGE_REGEX, $age)){
                    print( "<p>Niepoprawny format wieku</p>" );
                    $isError = true;
                }
                if(empty($name)){
                    print( "<p>Niepoprawne imię</p>" );
                    $isError = true;
                }
                if(empty($lastname)){
                    print( "<p>Niepoprawne nazwisko</p>" );
                    $isError = true;
                }
                if(empty($login)){
                    print( "<p>Niepoprawny login</p>" );
                    $isError = true;
                }
                if(empty($password)){
                    print( "<p>Niepoprawne hasło</p>" );
                    $isError = true;
                }



                if(!$isError && $_POST['data_update'] == "Zapisz"){
                    if ( !( $database = mysqli_connect( "localhost" , "root", null) ) )
                    {
                        die( "<p>Could not connect to database</p></body></html>" );
                    }
                    
                    if (!mysqli_select_db($database, "webpage_users" ))
                    {
                        die( "<p>Could not open webpage_users database</p></body></html>" );
                    }
                    $updateQuery = "UPDATE users SET ".
                                    "Name = '" . $_POST['user_name'] . "', " .
                                    "Lastname = '" . $_POST['user_lastname'] . "', " .
                                    "Email = '" . $_POST['user_email'] . "', " .
                                    "PhoneNum = '" . $_POST['user_phone'] . "', " .
                                    "Age = " . $_POST['user_age'] . ", " .
                                    "Password = '" . md5($_POST['user_pass']) . "' " .
                                    "WHERE Login = '" . $_SESSION['user_login'] ."';";
                    if(!mysqli_query( $database, $updateQuery)){
                        echo "aktualizacja blad";
                    };
                    mysqli_close($database);
                    print( "<h3>Dane użytkownika '{$_POST['user_login']}' zostały zapisane.</h3>" );
                }
                elseif(!$isError && $_POST['data_update'] == "Zarejestruj"){
                    if ( !( $database = mysqli_connect( "localhost" , "root", null) ) )
                    {
                        die( "<p>Could not connect to database</p></body></html>" );
                    }
                    
                    if (!mysqli_select_db($database, "webpage_users" ))
                    {
                        die( "<p>Could not open webpage_users database</p></body></html>" );
                    }

                    $createQuery = "INSERT INTO users (Name, Lastname, Email, PhoneNum, Age, Login, Password) 
                                    VALUES ('" . 
                                    $_POST['user_name'] . "', '" . 
                                    $_POST['user_lastname'] . "', '" . 
                                    $_POST['user_email'] . "', '" .
                                    $_POST['user_phone'] . "', " .
                                    $_POST['user_age'] . ", '" .
                                    $_POST['user_login'] . "', '" .
                                    md5($_POST['user_pass']) . "');";

                    if(!mysqli_query( $database,  $createQuery)){
                        echo mysqli_error($database);
                    };
                    mysqli_close($database);
                    print( "<h3>'{$_POST['user_login']}' dodany do bazy danych.</h3>" );
                }
            }
            elseif(isset($_POST['data_delete'])){
                if ( !( $database = mysqli_connect( "localhost" , "root", null) ) )
                    {
                        die( "<p>Could not connect to database</p></body></html>" );
                    }
                    
                    if (!mysqli_select_db($database, "webpage_users" ))
                    {
                        die( "<p>Could not open webpage_users database</p></body></html>" );
                    }
                    $deleteQuery = "DELETE FROM users WHERE Login = '" . $_SESSION['user_login'] . "';";

                    if(!mysqli_query( $database,  $deleteQuery)){
                        echo "usuwanie blad";
                    };
                    mysqli_close($database);
                    $userNameTmp = $_SESSION['user_login'];
                    session_destroy();
                    die("<h3>Dane użytkownika '{$userNameTmp}' zostały usunięte</h3>
                        <p><a class='goToMain' href='index.php'>Powrót do strony głownej</a></p></body></html>");
            }

            // $user_login = "";
            // $user_name = "";
            // $user_lastname = "";
            // $user_email = "";
            // $user_phone = "";
            // $user_age = "";
            // $user_pass = "";
            
            $form_array = array(
                                "user_pass" => "Hasło: ",
                                "user_name" => "Imię: ",
                                "user_lastname" => "Nazwisko: ",
                                "user_email" => "Email: ",
                                "user_phone" => "Numer tel.: ",
                                "user_age" => "Wiek: "
                            );

            if ( !( $database = mysqli_connect( "localhost" , "root", null) ) )
            {
                die( "<p>Nie można nawiązać połączenia z bazą danych.</p></body></html>" );
            }
            
            if (!mysqli_select_db($database, "webpage_users" ))
            {
                die( "<p>Nie można nawiązać połączenia z bazą danych.</p></body></html>" );
            }
            print("<h2>Użytkownik '{$_POST['user_login']}'</h2>");
            $result = mysqli_query( $database, "SELECT * FROM users WHERE Login=\"{$_POST['user_login']}\";" );
            if ( mysqli_num_rows($result) >= 1 )
            {
                $assocArray = mysqli_fetch_assoc($result);
                if($assocArray['Password'] != md5($_POST['user_pass'])){
                    die("<h3>Podane hasło użytkownika '{$_POST['user_login']}' jest niepoprawne.</h3>
                        <p><a class='goToMain' href='index.php'>Powrót do strony głownej</a></p></body></html>");
                }
                $_SESSION['user_login'] = $_POST["user_login"];
                $_SESSION['session'] = true;
                print( "<h3>'{$_POST['user_login']}' jest zalogowany.</h3>" );
                $user_name = $assocArray['Name'];
                $user_lastname = $assocArray['Lastname'];
                $user_email = $assocArray['Email'];
                $user_phone = $assocArray['PhoneNum'];
                $user_age = $assocArray['Age'];
                $user_pass = $_POST['user_pass'];              
            }
            else{
                print( "<h3>'{$_POST['user_login']}' nie istnieje w bazie danych. Wymagana rejestracja.</h3>" );
                unset($_SESSION['user_login']);
                $_SESSION['session'] = false;
            }
            mysqli_close($database);   

            print('<form name="user_data_form" method="post" action="http://localhost:80/phpLab9/sesja.php">');

            foreach($form_array as $key => $desc){
                print('<div><label>'.$desc.'</label><input type="text" name="'.$key.'" value="'. $$key.'"></div>');
            }

            if($_SESSION['session']){
                print('<div><input type="submit" value="Usuń" name="data_delete"></div>');
            }
            print('<input type="hidden" name="user_login" value="'
                    . $_POST['user_login'].'"<div><input type="submit" value='
                    . ($_SESSION['session']? "Zapisz" : "Zarejestruj").' name="data_update"></div></form>');
        ?>
        <p><a class="goToMain" href="index.php">Powrót do strony głownej</a></p>
    </body>
</html>