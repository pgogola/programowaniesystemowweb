<?php
    $query = "DROP DATABASE IF EXISTS webpage_users;";
    
    if ( !( $database = mysqli_connect( "localhost" , "root", null) ) )
    {
        die( "<p>Could not connect to database</p>" );
    }
    
    if ( !$database->select_db( "webpage_users" ) )
    {
        die( "<p>Could not open webpage_users database</p>" );
    }   
    
    if ( $result = mysqli_query( $database, $query) )
    {
        print( "<p>Database deleted!</p>" );
    }
    else{
        print( "<p>Could not execute query!</p>" );
        die( mysql_error() );
    }
    mysqli_close($database);
?>